import numpy
from libKMCUDA import kmeans_cuda

numpy.random.seed(0)
arr = numpy.empty((100000, 2), dtype=numpy.float32)
arr[:25000] = numpy.random.rand(25000, 2) + [0, 2]
arr[25000:50000] = numpy.random.rand(25000, 2) - [0, 2]
arr[50000:75000] = numpy.random.rand(25000, 2) + [2, 0]
arr[75000:] = numpy.random.rand(25000, 2) - [2, 0]
centroids, assignments = kmeans_cuda(arr, 40, verbosity=1, seed=3)
print(centroids)

#from matplotlib import pyplot
#pyplot.scatter(arr[:, 0], arr[:, 1], c=assignments)
#pyplot.scatter(centroids[:, 0], centroids[:, 1], c="white", s=150)
