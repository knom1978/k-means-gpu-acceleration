FROM nvidia/cuda:9.2-cudnn7-devel-ubuntu18.04
LABEL maintainer="Cecil Liu"
ENV LD_LIBRARY_PATH /usr/local/cuda-9.2/lib64:/usr/local/cuda-9.2/extras/CUPTI/lib64:$LD_LIBRARY_PATH

RUN apt-get update --fix-missing && \
    apt-get install -y wget bzip2 ca-certificates curl git libgtk2.0-dev ffmpeg libsm6 libxext6 && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

# Install anaconda for python 3.9
RUN wget --quiet https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh -O ~/miniconda.sh && \
    /bin/bash ~/miniconda.sh -b -p /opt/conda && \
    rm ~/miniconda.sh && \
    /opt/conda/bin/conda clean -tipsy && \
    ln -s /opt/conda/etc/profile.d/conda.sh /etc/profile.d/conda.sh && \
    echo ". /opt/conda/etc/profile.d/conda.sh" >> ~/.bashrc && \
    echo "conda activate base" >> ~/.bashrc

ENV PATH /opt/conda/bin:$PATH    
        
RUN conda update conda
RUN conda install python=3.7

RUN conda install pip && \
    conda install pytorch==1.7.1 torchvision==0.8.2 torchaudio==0.7.2 cudatoolkit=9.2 -c pytorch

#https://github.com/subhadarship/kmeans_pytorch
RUN pip install --upgrade pip setuptools && \
    pip install --no-cache-dir scikit-learn matplotlib scikit-image scipy opencv-contrib-python numpy pandas Pillow h5py && \
    pip install --no-cache-dir lxml cython pycocotools torchsummary torchinfo kmeans-pytorch opencv-contrib-python

#https://github.com/facebookresearch/faiss/blob/main/INSTALL.md
RUN conda install -c conda-forge faiss-gpu

ENV CUDA_TOOLKIT_ROOT_DIR /usr/local/cuda-9.2:$CUDA_TOOLKIT_BOOT_DIR
ENV CUDA_INCLUDE_DIRS /usr/local/cuda-9.2/include

#https://github.com/src-d/kmcuda
#970-5.2
#1080-6.1
#2080-7.5
#P100-6.0
RUN pip install cmake
RUN git clone https://github.com/src-d/kmcuda
WORKDIR /kmcuda/src/
RUN cmake -DCMAKE_BUILD_TYPE=Release -D DISABLE_R=y -D CUDA_ARCH=52 -D CUDA_TOOLKIT_ROOT_DIR=/usr/local/cuda-9.2  . && make
RUN CUDA_ARCH=52 CUDA_TOOLKIT_ROOT_DIR=/usr/local/cuda-9.2 pip install libKMCUDA

RUN useradd -rm -d /home/ubuntu -s /bin/bash -g root -G sudo -u 1000 ubuntu
USER ubuntu
WORKDIR /home/ubuntu

# Set the default command to python3
CMD ["python3"]

