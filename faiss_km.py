#! /usr/bin/python
import torch
import faiss
# where the magic happens
import faiss.contrib.torch_utils
dims = 128
nlist = 1000
res = faiss.StandardGpuResources()
index = faiss.GpuIndexIVFFlat(res, dims, nlist, faiss.METRIC_L2)
train_vecs = torch.rand(100000, dims, device=torch.device('cuda', 0))
index.train(train_vecs)
add_vecs = torch.rand(1000000, dims, device=torch.device('cuda', 0))
index.add(add_vecs)
query_vecs = torch.rand(5, dims, device=torch.device('cuda', 0))
distances, indices = index.search(query_vecs, 5)
print(distances)
print(indices)
