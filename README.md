# K-means GPU acceleration
This **Image/Dockerfile** aims to create containers for accelerating K-means

## GPU-based Kmeans
* K-means by pytorch (kmeans_pytorch)
* K-means by faiss
* K-means by libkmcuda
* K-meabs by Rapids (cuml)

## How to use?
* You should git clone this repository first.
* And then cd into the folder, type as follows.
```
sudo docker build -t pytorch_env/kmeansgpu:1.0 .
```
* For your scripts, you can use as the hereunder part. You should run as :
```
sudo docker run -it --rm -v "$PWD":/home/ubuntu -w /home/ubuntu pytorch_env/kmeansgpu:1.0 python pytorch_km.py
sudo docker run -it --rm -v "$PWD":/home/ubuntu -w /home/ubuntu pytorch_env/kmeansgpu:1.0 python faiss_km.py
sudo docker run -it --rm -v "$PWD":/home/ubuntu -w /home/ubuntu pytorch_env/kmeansgpu:1.0 python kmcuda_km.py
sudo docker run -it --rm -v "$PWD":/home/ubuntu -w /home/ubuntu rapidsai/rapidsai:cuda10.0-runtime-ubuntu18.04-py3.7 python rapids_km.py
```
